module Forem
  class Topic < ActiveRecord::Base
		before_save :set_post_user

  	has_many :posts, :order => "created_at ASC"
    accepts_nested_attributes_for :posts

    has_one :last_post, :class_name => "Forem::Post",
                        :order => "created_at DESC"

    belongs_to :user, :class_name => Forem::Engine.user_class.to_s

    attr_accessible :subject, :user_id, :posts_attributes, :user

    private
    	def set_post_user
    		self.posts.first.user = self.user
    	end

  end
end
