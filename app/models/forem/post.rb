module Forem
  class Post < ActiveRecord::Base
  	belongs_to :topic, :counter_cache => true
    attr_accessible :text, :topic_id, :user_id, :user
    
    belongs_to :user, :class_name => Forem::Engine.user_class.to_s

  end
end
